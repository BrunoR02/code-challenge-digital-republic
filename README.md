Como rodar e iniciar o projeto:

1. Precisa ter o Node instalado no computador.
2. Fazer o clone do projeto no seu computador pelo git:   
  -Abrir git bash ou terminal com git instalado no computador.  
  -Navegar até a pasta onde quer ter o projeto clonado.  
  -Rodar comando: git clone https://gitlab.com/BrunoR02/code-challenge-digital-republic.git
3. Entrar na pasta do projeto pelo terminal.
2. Rodar no terminal: "npm install" para instalar as dependências.
3. Rodar no terminal: "npm start" para iniciar o projeto no localhost.
