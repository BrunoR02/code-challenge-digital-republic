import { useReducer } from "react"

function inputValueReducer(state,action){
    if(action.input === "height"){
        return {...state, height: action.value}
    } else if(action.input === "width"){
        return {...state, width: action.value}
    } else if(action.input === "doors"){
        return {...state, doors: action.value}
    } else if(action.input === "windows"){
        return {...state, windows: action.value}
    } 
}

export default function useInput(){
    const [inputValue, dispatchValue] = useReducer(inputValueReducer,{
        height: 0,
        width: 0,
        doors: 0,
        windows: 0, 
    }) 

    function heightChangeHandler(event){
        let value = event.target.value > 25 ? 25 : (event.target.value < 0 ? 0 : event.target.value) 
        dispatchValue({input: 'height', value: parseFloat(value) || 0 })
    }

    function widthChangeHandler(event){
        let value = event.target.value > 25 ? 25 : (event.target.value < 0 ? 0 : event.target.value) 
        dispatchValue({input: 'width', value: parseFloat(value) || 0})
    }

    function doorsChangeHandler(event){
        let value = event.target.value > 15 ? 15 : (event.target.value < 0 ? 0 : event.target.value) 
        dispatchValue({input: 'doors', value: parseFloat(value) || 0})
    }

    function windowsChangeHandler(event){
        let value = event.target.value > 10 ? 10 : (event.target.value < 0 ? 0 : event.target.value) 
        dispatchValue({input: 'windows', value: parseFloat(value) || 0})
    }

    return {
        value: inputValue,
        heightChangeHandler,
        widthChangeHandler,
        doorsChangeHandler,
        windowsChangeHandler,
    }
}