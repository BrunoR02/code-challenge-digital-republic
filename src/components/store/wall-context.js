import { createContext, useCallback, useEffect, useState } from "react";


const WallContext = createContext({
    areaTotal: 0,
    paintList: {},
    setArea: (wallIndex,value)=>{},
    setPaint: (list)=>{},
    resetAll: ()=>{},
})


export function WallContextProvider(props){
    const [areas,setAreas] = useState([0,0,0,0])
    const [areaTotal, setAreaTotal] = useState(0)
    const [paintList, setPaintList] = useState({
        small: 0,
        medium: 0,
        large: 0,
        extra: 0
    })

    const setPaint = useCallback((list) =>{
        setPaintList(list)
    },[])

    const setArea = useCallback((wallIndex, value)=>{
        setAreas((prevList)=>prevList.map((item,index)=>index===(wallIndex-1) ? value : item))
    },[])

    const resetAll = useCallback(()=>{
        setAreas([0,0,0,0])
        setAreaTotal(0)
        setPaintList({
            small: 0,
            medium: 0,
            large: 0,
            extra: 0
        })
    },[])

    useEffect(()=>{
        if(!(areas.some(item=>item===0))){
            let total = 0;
            areas.forEach(item=>{total += item})
            setAreaTotal(Math.round(total))
        } else if(areas.some(item=>item!==0)){
            resetAll()
        }
    },[areas,resetAll])

    const context = {
        areaTotal,
        paintList,
        setArea,
        setPaint,
        resetAll,
    }

    return(
        <WallContext.Provider value={context}>
            {props.children}
        </WallContext.Provider>
    )
}

export default WallContext