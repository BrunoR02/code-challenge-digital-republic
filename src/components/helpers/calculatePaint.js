
export default function calculatePaint(areaTotal){
    let totalPaint = (areaTotal / 5)
    const can = {
        small: 0, // Lata de 0,5 L
        medium: 0, // Lata de 2,5 L
        large: 0, // Lata de 3,6 L
        extra: 0, // Lata de 18 L
    } 

    while(totalPaint > 0){
        if(totalPaint > 18){
            totalPaint -= 18
            can.extra++
        } else if(totalPaint > 3.6){
            totalPaint-= 3.6
            can.large++
        } else if(totalPaint > 2.5){
            totalPaint -= 2.5
            can.medium++
        } else if(totalPaint > 0.5){
            totalPaint-= 0.5
            can.small++
        } else {
            totalPaint = 0
            can.small++
        }
    }
    
    return can
}