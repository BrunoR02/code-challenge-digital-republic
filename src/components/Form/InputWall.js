import {useContext, useEffect, useState} from "react"
import useInput from "../hooks/useInput"
import WallContext from "../store/wall-context"

import classes from "./InputWall.module.css"

export default function InputWall(props){
    const inputs = useInput()
    const {setArea} = useContext(WallContext)
    const [error,setError] = useState("")

    const {submited,index} = props

    let hasError = !!(error !== "") 

    useEffect(()=>{
        if(submited){

            let areaTotal = (inputs.value.height * inputs.value.width)
            let areaValid = areaTotal - (inputs.value.doors * (0.8 * 1.9)) - (inputs.value.windows * (2 * 1.2))

            let errorMessage = "";
            
            if(areaTotal === 0){
                errorMessage = "A parede não pode ter altura ou largura igual a 0."
            } else if(areaTotal > 50 || areaTotal < 1){
                errorMessage = "A parede não pode ter mais que 50 ou menos que 1 metros quadrados."
            } else if(inputs.value.doors && inputs.value.height < 2.2){
                errorMessage = "A altura da parede precisa ser pelo menos 30 centímetros maior do que a de uma porta(2,2 metros mínimo)"
            } else if(areaValid < (areaTotal/2)){
                errorMessage = "O numero de portas e janelas só podem ocupar até metade da área da parede."
            } else {
                setArea(index, areaValid)
            }    
            setError(errorMessage)
        }
        // eslint-disable-next-line
    },[submited,setArea]) 


    return(
        <div className={classes.inputControl}>
            <h4 className={classes.inputTitle}>Parede {index}:</h4>

            <label className={classes.label} htmlFor="height">Largura (em metros)</label>
            <input className={classes.input} type="number" name="width" max="25" step="0.1" placeholder="Largura" value={inputs.value.width} onChange={inputs.widthChangeHandler}/>
            
            <label className={classes.label} htmlFor="height">Altura (em metros)</label>
            <input className={classes.input} type="number" name="height" max="25" step="0.1" placeholder="Altura" value={inputs.value.height} onChange={inputs.heightChangeHandler}/>
            
            <label className={classes.label} htmlFor="height">Número de portas</label>
            <input className={classes.input} type="number" name="door" max="15" placeholder="Núm Portas" value={inputs.value.doors} onChange={inputs.doorsChangeHandler}/>
            
            <label className={classes.label} htmlFor="height">Número de janelas</label>
            <input className={classes.input} type="number" name="window" max="10" placeholder="Núm Janelas" value={inputs.value.windows} onChange={inputs.windowsChangeHandler}/>

            {hasError && <span className={classes.error}>{error}</span>}
        </div>
    )
}