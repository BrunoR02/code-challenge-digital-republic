import { useContext, useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import InputWall from "./InputWall"
import LoadingSpinner from "../LoadingSpinner"
import WallContext from "../store/wall-context"
import calculatePaint from "../helpers/calculatePaint"

import classes from "./Form.module.css"

export default function Form(){
    const [submited, setSubmited] = useState(false)
    const [isLoading,setIsLoading] = useState(false)
    const {areaTotal,setPaint,resetAll} = useContext(WallContext)

    const navigate = useNavigate()

    function submitHandler(event){
        event.preventDefault()
        setSubmited(true)
        setIsLoading(true)
        setTimeout(()=>{
            setSubmited(false)
            setIsLoading(false)
        },500)
    }

    useEffect(()=>{
        if(areaTotal){
            setPaint(calculatePaint(areaTotal))
            setSubmited(false)
            setIsLoading(false)
            navigate("/result")
        } else {
            resetAll()
        }
    },[areaTotal,setPaint,navigate,resetAll]) 

    return (
        <>
            {isLoading && <LoadingSpinner/>}
            <form onSubmit={submitHandler} className={classes.form}>
                <section className={classes.inputSection}>
                    <InputWall submited={submited} index={1}/>
                    <InputWall submited={submited} index={2}/>
                    <InputWall submited={submited} index={3}/>
                    <InputWall submited={submited} index={4}/>   
                </section>
                <button className={classes.button}>Calcular</button>
            </form>
        </>
    )
}