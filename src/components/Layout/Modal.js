import classes from "./Modal.module.css"

export default function Modal(props){
    return (
        <section className={classes.modal}>
            {props.children}
        </section>
    )
}