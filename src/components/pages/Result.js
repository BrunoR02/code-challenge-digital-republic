import { useContext, useEffect, useState} from "react"
import {useNavigate} from "react-router-dom"
import Modal from "../Layout/Modal"
import WallContext from "../store/wall-context"

import classes from "./Result.module.css"


export default function Result(){
    const {paintList,resetAll} = useContext(WallContext)
    const [paints, setPaints] = useState({})
    const navigate = useNavigate() 

    useEffect(()=>{
        setPaints(paintList)
        resetAll()
        //eslint-disable-next-line
    },[])

    return(
        <section className={classes.container}>
            <h2 className={classes.title}>Resultado:</h2>
            <Modal>
                <p className={classes.desc}>Você irá precisar de:<br/></p>
                <ul className={classes.list}>
                    {paints.extra > 0 && <li>{paints.extra} {paints.extra === 1 ? "lata" : "latas"} de 18 L</li>}
                    {paints.large > 0 && <li>{paints.large} {paints.large === 1 ? "lata" : "latas"} de 3,6 L</li>}
                    {paints.medium > 0 && <li>{paints.medium} {paints.medium === 1 ? "lata" : "latas"} de 2,5 L</li>}
                    {paints.small > 0 && <li>{paints.small} {paints.small === 1 ? "lata" : "latas"} de 0,5 L</li>}
                </ul>
                <button onClick={()=>{navigate("/")}} className={classes.button}>Voltar a página inicial.</button>
            </Modal>
        </section>
    )
}