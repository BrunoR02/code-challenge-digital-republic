import Form from "../Form/Form"
import Modal from "../Layout/Modal"

import classes from "./Home.module.css"

export default function Home(){

    return (
        <div className={classes.container}>
            <h2 className={classes.title}>Calculo de Tintas</h2>
            <p className={classes.desc}>Descubra agora quantas latas de tintas você irá precisar 
            para pintar a sua sala! Apenas informe as medidas pedidas abaixo e você saberá.<br/>
            Detalhes de dimensões:<br/> Porta - 0,80 x 1,90 metros (1,52m²)<br/> Janela - 2,00 x 1,20 metros (2,40m²)
            </p>
            <Modal>
                <Form/>
            </Modal>

        </div>
    )
}