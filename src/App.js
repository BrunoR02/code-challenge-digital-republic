import {Routes,Route} from "react-router-dom"
import Home from './components/pages/Home';
import Result from "./components/pages/Result";
import { WallContextProvider } from './components/store/wall-context';

export default function App() {
  return (
    <WallContextProvider>
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/result" element={<Result/>}/>
        </Routes>
    </WallContextProvider>
  );
}